package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HeadsetsPage extends BasePage {

    @FindBy(xpath = "//img[@data-image-index='1']")
    //@FindBy(xpath = "//div[@data-component-id='1']//span[contains(@class,'a-size-medium')]")
    private WebElement firstProductInHeadsetCategory;

    @FindBy(xpath = "//span[@class='a-price-symbol']")
    private WebElement currencyType;

    @FindBy(xpath = "//span[@class='a-price-symbol']")
    private List<WebElement> currencyTypeList;


    @FindBy(xpath = "//a[@id='p_n_deal_type/23566064011']")
    private WebElement todayDealsOption;

    public HeadsetsPage(WebDriver driver) {
        super(driver);
    }
    public String getFirstCurrencyType() {
       return currencyTypeList.get(0).getText();
    }

    public WebElement getFirstProductInHeadsetCategory() {
        return firstProductInHeadsetCategory;
    }

    public String getCurrencyTypeText() {
        return currencyType.getText();
    }

    public void clickFirstProductInHeadsetCategory() {
        firstProductInHeadsetCategory.click();
    }

    public void clickTodayDealsOption() {
        todayDealsOption.click();
    }

}

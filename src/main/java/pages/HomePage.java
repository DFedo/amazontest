package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//header[contains(@id,'navbar-main')]")
    private WebElement header;

    @FindBy(xpath = "//div[contains(@id,'navFooter')]")
    private WebElement footer;

    @FindBy(xpath = "//a[contains(@id,'nav-cart')]")
    private WebElement cartIcon;

    @FindBy(xpath = "//a[contains(@id,'icp-nav-flyout')]")
    private WebElement languageButton;

    @FindBy(xpath = "//div[contains(@class,'nav-sprite hmenu')]")
    private WebElement storeMenuCloseButton;

    @FindBy(xpath = "//a[contains(@id,'nav-link-accountList')]")
    private WebElement signInButton;

    @FindBy(xpath = "//input[contains(@id,'twotabsearchtextbox')]")
    private WebElement searchField;

    @FindBy(xpath = "//input[contains(@id,'nav-search-submit-button')]")
    private WebElement searchButton;

    @FindBy(xpath = "//span[contains(@id,'glow-ingress-line2')]")
    private WebElement destinationCountryButton;

    @FindBy(xpath = "//span[contains(@id,'GLUXCountryListDropdown')]")
    private WebElement countrySelectionDropdown;

    @FindBy(xpath = "//li[contains(@aria-labelledby,'GLUXCountryList_234')]")
    private WebElement unitedKingdomRadioButton;

    @FindBy(xpath = "//button[contains(@name,'glowDoneButton')]")
    private WebElement submitDestinationButton;

    @FindBy(xpath = "//a[contains(@id,'nav-hamburger-menu')]")
    private WebElement storeMenuButton;

    @FindBy(xpath = "//div[contains(@id,'hmenu-content')]")
    private WebElement storeMenuTitle;

    @FindBy(xpath = "//span[contains(@class,'a-button-text a-declarative')]")
    private WebElement destinationCountryMenu;

    @FindBy(xpath = "//span[contains(@id,'nav-link')]")
    private WebElement loggedInUserGreetings;

    @FindBy(xpath = "//a[contains(@data-nav-role,'signin')]")
    private WebElement accountsAndListsButton;

    @FindBy(xpath = "//span[text()='Sign Out']")
    private WebElement signOutButton;

    @FindBy(xpath = "//div[@id='desktop-grid-1']//a[contains(@aria-label,'Headset')]")
    private WebElement headsetCategory;

    @FindBy(xpath = "//span[text()='Create a List']")
    private WebElement createAListButton;

    @FindBy(xpath = "//span[text()='Shopping List']")
    private WebElement createAShoppingListButton;

    @FindBy(xpath = "//header[contains(@class,'a-popover-header')]")
    private WebElement destinationCountryHeader;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getDestinationCountryHeader() {
        return destinationCountryHeader;
    }

    public WebElement getStoreMenuTitle() {
        return storeMenuTitle;
    }

    public WebElement getUnitedKingdomRadioButton() {
        return unitedKingdomRadioButton;
    }

    public WebElement getDestinationCountryMenu() {
        return destinationCountryMenu;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getCreateAShoppingListButton() {
        return createAShoppingListButton;
    }

    public WebElement getCartIcon() {
        return cartIcon;
    }

    public String getDestinationCountryText() {
        return destinationCountryButton.getText();
    }

    public String getLoggedInUserGreetingsText() {
        return loggedInUserGreetings.getText();
    }

    public boolean isHeaderVisible() {
        return header.isDisplayed();
    }

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }

    public boolean isDestinationCountryMenuVisible() {
        return destinationCountryButton.isDisplayed();
    }

    public boolean isLanguageButtonVisible() {
        return languageButton.isDisplayed();
    }

    public boolean isSignInButtonVisible() {
        return signInButton.isDisplayed();
    }

    public boolean isStoreMenuTitleVisible() {
        return storeMenuTitle.isDisplayed();
    }

    public void clickSignInButton() {
        signInButton.click();
    }

    public void clickCreateAShoppingListButton() {
        createAShoppingListButton.click();
    }

    public void clickUnitedKingdomRadioButton() {
        unitedKingdomRadioButton.click();
    }

    public void clickCreateAListButton() {
        createAListButton.click();
    }

    public void clickDestinationCountryButton() {
        destinationCountryButton.click();
    }

    public void clickDestinationCountryMenu() {
        destinationCountryMenu.click();
    }

    public void clickSignOutButton() {
        signOutButton.click();
    }

    public void hoverOverAccountsAndListsButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(accountsAndListsButton).build().perform();
    }

    public void clickStoreMenuButton() {
        storeMenuButton.click();
    }

    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    public void clickCartButton() {
        cartIcon.click();
    }

    public void clickSearchButton() {
        searchButton.click();
    }


    public void clickLanguageButton() {
        languageButton.click();
    }

    public void clickHeadsetCategory() {
        headsetCategory.click();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickStoreMenuCloseButton() {
        storeMenuCloseButton.click();
    }

    public void clickSubmitDestinationButton() {
        submitDestinationButton.click();
    }

    public void openHomePage(String url) {
        driver.get(url);
    }
}

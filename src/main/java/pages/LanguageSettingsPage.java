package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LanguageSettingsPage extends BasePage{

    @FindBy(xpath = "//input[@value='de_DE']//following-sibling::span")
    private WebElement germanLanguageButton;

    @FindBy(xpath = "//span[@class='a-button-text a-declarative']")
    private WebElement dropDownForCurrencyChange;

    @FindBy(xpath = "//li[@id='EUR']")
    private WebElement currencyEuro;

    @FindBy(xpath = "//input[contains(@class,'a-button-input')]")
    private WebElement saveChangesButton;

    @FindBy(xpath = "//div[@id='icp-language-headings']")
    private WebElement languageSettingsTitle;

    public LanguageSettingsPage(WebDriver driver) {
        super(driver);
    }

    public void clickDropDownForCurrencyChange() {
        dropDownForCurrencyChange.click();
    }

    public void clickGermanLanguageButton() {
        germanLanguageButton.click();
    }

    public void clickCurrencyEuroButton() {
        currencyEuro.click();
    }

    public void clickSaveChangesButtonButton() {
        saveChangesButton.click();
    }

    public WebElement getLanguageSettingsTitle() {
        return languageSettingsTitle;
    }

}

package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {
    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[contains(@id,'attach-sidesheet-view-cart-button-announce')]")
    private WebElement goToCartButton;

    @FindBy(xpath = "//input[contains(@id,'add-to-wishlist-button-submit')]")
    private WebElement addToListButton;

    @FindBy(xpath = "//div[contains(@class,'w-success-msg')]")
    private WebElement successfulAddingToListMessage;

    @FindBy(xpath = "//a[@id='WLHUC_viewlist']")
    private WebElement viewListButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public boolean isSuccessfulAddingToListMessageVisible() {
        return successfulAddingToListMessage.isDisplayed();
    }

    public void clickAddToListButton() {
        addToListButton.click();
    }

    public WebElement getSuccessfulAddingToListMessage() {
        return successfulAddingToListMessage;
    }

    public void clickViewListButton() {
        viewListButton.click();
    }

    public void clickAddToCartButton() {
        addToCartButton.click();
    }

    public void clickGoToCartButton() {
        goToCartButton.click();
    }

    public void clickGoToCartButtonJs() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", goToCartButton);
    }

}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage {
    @FindBy(xpath = "//div[contains(@class,'a-row sc-your-amazon-cart-is-empty')]")
    private WebElement cartIsEmptyMessage;

    @FindBy(xpath = "//span[contains(@id,'sc-subtotal-label-activecart')]")
    private WebElement quantityOfProductsInCart;

    @FindBy(xpath = "//input[@value='Delete']")
    private WebElement deleteProductFromCartButton;

    @FindBy(xpath = "//div[contains(@class,'a-row sc-cart-header')]")
    private WebElement cartHeader;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCartEmptyMessageVisible() {
        return cartIsEmptyMessage.isDisplayed();
    }

    public void clickDeleteProductFromCartButton() {
        deleteProductFromCartButton.click();
    }
    public WebElement getCartIsEmptyMessage() {
        return cartIsEmptyMessage;
    }

    public WebElement getCartHeader() {
        return cartHeader;
    }

    public String getQuantityOfProductsInCart() {
        return quantityOfProductsInCart.getText();
    }


}

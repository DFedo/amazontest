package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id='continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//input[@id='signInSubmit']")
    private WebElement signInSubmitButton;

    @FindBy(xpath = "//h4[contains(@class,'a-alert-heading')]")
    private WebElement alertMessage;

    @FindBy(xpath = "//a[contains(@id,'ap_change_login_claim')]")
    private WebElement changeEmailButton;

    @FindBy(xpath = "//h1[contains(@class,'a-spacing-small')]")
    private WebElement signInPageTitle;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public WebElement alertMessage() {
        return alertMessage;
    }

    public WebElement emailField() {
        return emailField;
    }

    public void clickContinueButton() {
        continueButton.click();
    }

    public void clickSignInSubmitButton() {
        signInSubmitButton.click();
    }

    public void clickChangeEmailButton() {
        changeEmailButton.click();
    }

    public boolean isAlertMessageDisplayed() {
        return alertMessage.isDisplayed();
    }

    public boolean isSignInPageTitle() {
        return signInPageTitle.isDisplayed();
    }

    public void enterTextToEmailField(final String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void enterTextToPasswordField(final String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }


}



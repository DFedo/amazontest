package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class WishlistPage extends BasePage {

    @FindBy(xpath = "//input[@name='submit.deleteItem']")
    private WebElement deleteItemFromListButton;

    @FindBy(xpath = "//div[text()='Deleted']")
    private WebElement itemDeletedMessage;

    @FindBy(xpath = "//div[contains(@class,'a-row a-spacing-mini')]")
    private WebElement ideaListTitle;

    @FindBy(xpath = "//span[text()='Create List']")
    private WebElement createListSubmitButton;

    @FindBy(xpath = "//div[contains(@class,'wl-list selected')]")
    private WebElement shoppingListCreatedTitle;

    @FindBy(xpath = "//header[@class='a-popover-header']")
    private WebElement newListMenuHeader;

    @FindBy(xpath = "//div[contains(@class,'a-section a-spacing-base dl-inappropriat')]")
    private WebElement ideaListErrorMessage;

    @FindBy(xpath = "//a[@id='overflow-menu-popover-trigger']")
    private WebElement moreOptionsButton;

    @FindBy(xpath = "//li[@id='awl-delete_item_action-edit']")
    private WebElement manageListButton;

    @FindBy(xpath = "//a[@id='editYourList'] click")
    private WebElement editListButton;

    @FindBy(xpath = "//button[contains(@class,'jetset-button j4t-button')]")
    private WebElement createIdeaListButton;

    @FindBy(xpath = "//textarea[contains(@id,'reg-create-list-name-dl-input')]")
    private WebElement ideaListNameField;

    @FindBy(xpath = "//input[@id='list-settings-name']")
    private WebElement listNameFieldInput;

    @FindBy(xpath = "//span[@id='list-settings-save-announce']")
    private WebElement saveChangesButton;

    @FindBy(xpath = "//a[text()='Your Idea Lists']")
    private WebElement ideaListButton;

    @FindBy(xpath = "//span[text()='Delete list']")
    private WebElement deleteList;

    @FindBy(xpath = "//input[@name='submit.save']")
    private WebElement submitSaveButton;

    @FindBy(xpath = "//button[contains(@class,'submit')]")
    private WebElement ideaListSubmitButton;

    @FindBy(xpath = "//span[contains(@data-action,'create-list-submit')]")
    private WebElement createNewListSubmitButton;

    public WishlistPage(WebDriver driver) {
        super(driver);
    }

    public String getShoppingListCreatedTitleText() {
        return shoppingListCreatedTitle.getText();
    }

    public void clickDeleteItemFromListButton() {
        deleteItemFromListButton.click();
    }

    public void clickIdeaListButton() {
        ideaListButton.click();
    }

    public void clickCreateIdeaListButton() {
        createIdeaListButton.click();
    }

    public void clickSubmitSaveButton() {
        submitSaveButton.click();
    }

    public void clickCreateNewListSubmitButton() {
        createNewListSubmitButton.click();
    }

    public void clickMoreOptionsButton() {
        moreOptionsButton.click();
    }

    public void clickIdeaListSubmitButton() {
        ideaListSubmitButton.click();
    }

    public void clickEditListButton() {
        editListButton.click();
    }

    public void enterTextToListName(final String newList) {
        listNameFieldInput.clear();
        listNameFieldInput.sendKeys(newList);
    }

    public void enterTextToNewIdeaListField(final String ideaListName) {
        ideaListNameField.clear();
        ideaListNameField.sendKeys(ideaListName);
    }

    public boolean isItemDeletedMessageVisible() {
        return itemDeletedMessage.isDisplayed();
    }

    public boolean isShoppingListCreatedTitleVisible() {
        return shoppingListCreatedTitle.isDisplayed();
    }

    public boolean isNewListMenuHeaderVisible() {
        return newListMenuHeader.isDisplayed();
    }

    public boolean isIdeaListErrorMessageVisible() {
        return ideaListErrorMessage.isDisplayed();
    }

    public WebElement getItemDeletedMessage() {
        return itemDeletedMessage;
    }

    public WebElement getIdeaListErrorMessage() {
        return ideaListErrorMessage;
    }

    public WebElement getMoreOptionsButton() {
        return moreOptionsButton;
    }

    public WebElement getIdeaListTitle() {
        return ideaListTitle;
    }

    public WebElement getNewListMenuHeader() {
        return newListMenuHeader;
    }

    public void hoverOverMoreOptionsButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(moreOptionsButton).build().perform();
    }


}

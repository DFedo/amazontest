package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.junit.Assert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.*;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    private static final long DEFAULT_TIMEOUT = 10;
    WebDriver driver;
    HomePage homePage;
    ShoppingCartPage shoppingCartPage;
    HeadsetsPage headsetsPage;
    LanguageSettingsPage languageSettingsPage;
    ProductPage productPage;
    SignInPage signInPage;
    WishlistPage wishlistPage;
    PageFactoryManager pageFactoryManager;


    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @And("User checks search field visibility")
    public void userChecksSearchFieldVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isSearchFieldVisible());
    }

    @When("User makes search by keyword {string}")
    public void userMakesSearchByKeyword(final String keyword) {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSearchField());
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void userClicksSearchButton() {
        homePage.clickSearchButton();
    }


    @Then("User checks that needed keyword {string} is present on the page")
    public void userChecksThatNeededQueryIsPresentOnThePage(final String keyword) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getPageSource().contains(keyword));
    }

    @And("User clicks language button")
    public void userClicksLanguageButton() {
        homePage.clickLanguageButton();
    }

    @And("User select currency")
    public void userSelectCurrency() {
        languageSettingsPage.clickDropDownForCurrencyChange();
        languageSettingsPage.clickCurrencyEuroButton();
    }

    @And("User clicks Save Changes Button")
    public void userClicksSaveChangesButton() {
        languageSettingsPage.clickSaveChangesButtonButton();
    }

    @When("User select language")
    public void selectLanguage() {
        languageSettingsPage = pageFactoryManager.getLanguageSettingsPage();
        languageSettingsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, languageSettingsPage.getLanguageSettingsTitle());
        languageSettingsPage.clickGermanLanguageButton();
    }

    @Then("User checks that Url contains {string} language")
    public void checkCurrentUrl(final String language) throws InterruptedException {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        Thread.sleep(2000);
       // homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSearchField());
        assertTrue(driver.getCurrentUrl().contains(language));
    }

    @And("User checks presence of {string} currency")
    public void checkCurrencyPresence(final String currency) {
        headsetsPage = pageFactoryManager.getHeadsetsPage();
        assertTrue(headsetsPage.getFirstCurrencyType().equalsIgnoreCase(currency));

    }

    @And("User checks header visibility")
    public void userChecksHeaderVisibility() {
        assertTrue(homePage.isHeaderVisible());
    }


    @And("User checks footer visibility")
    public void userChecksFooterVisibility() {
        assertTrue(homePage.isFooterVisible());
    }

    @And("User checks cart menu visibility")
    public void userChecksCartMenuVisibility() {
        assertTrue(homePage.isCartIconVisible());
    }

    @And("User checks sign in menu visibility")
    public void userChecksSignInMenuVisibility() {
        assertTrue(homePage.isSignInButtonVisible());
    }

    @And("User checks change language menu visibility")
    public void userChecksChangeLanguageMenuVisibility() {
        assertTrue(homePage.isLanguageButtonVisible());
    }

    @And("User checks deliver to button visibility")
    public void userChecksDeliverToButtonVisibility() {
        assertTrue(homePage.isDestinationCountryMenuVisible());
    }

    @When("User clicks Store menu")
    public void userClicksStoreMenu() {
        homePage.clickStoreMenuButton();
    }

    @Then("User checks that store menu is visible")
    public void userChecksThatStoreMenuIsVisible() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getStoreMenuTitle());
        assertTrue(homePage.isStoreMenuTitleVisible());
    }

    @And("User clicks cart button")
    public void userClicksCartButton() {
        homePage = pageFactoryManager.getHomePage();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickCartButton();
    }

    @And("User checks message that cart is empty")
    public void userChecksMessageThatCartIsEmpty() {
        shoppingCartPage = pageFactoryManager.getShoppingCartPage();
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(shoppingCartPage.isCartEmptyMessageVisible());
    }

    @And("User clicks close store menu button")
    public void userClicksCloseStoreMenuButton() {
        homePage.clickStoreMenuCloseButton();
    }

    @And("User clicks category Headset")
    public void userClicksCategoryHeadset() {
        homePage.clickHeadsetCategory();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on the first product")
    public void userClicksOnTheFirstProduct() {
        headsetsPage = pageFactoryManager.getHeadsetsPage();
        headsetsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        headsetsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, headsetsPage.getFirstProductInHeadsetCategory());
        headsetsPage.clickFirstProductInHeadsetCategory();
    }

    @When("User clicks add to cart button")
    public void userClicksAddToCartButton() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.clickAddToCartButton();
        try {
            productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        } catch (Exception e) {
            productPage.pressEscape();
        }
    }


    @Then("User checks  for message {string} about added to cart item")
    public void userChecksMessageAboutAddedToCartItem(final String message) {
        shoppingCartPage = pageFactoryManager.getShoppingCartPage();
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(shoppingCartPage.getQuantityOfProductsInCart(), message);
    }

    @And("User delete added item from cart")
    public void userDeleteAddedItemFromCart() {
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        shoppingCartPage.clickDeleteProductFromCartButton();
    }


    @And("User clicks today deal option")
    public void userClicksTodayDealOption() {
        headsetsPage = pageFactoryManager.getHeadsetsPage();
        headsetsPage.clickTodayDealsOption();
        headsetsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User checks for message {string} that item deleted")
    public void userChecksForMessageThatItemDeleted(final String message) {
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        shoppingCartPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, shoppingCartPage.getCartIsEmptyMessage());
        assertEquals(shoppingCartPage.getQuantityOfProductsInCart(), message);
    }

    @And("User clicks go to cart button")
    public void userClicksGoToCartButton() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.clickGoToCartButtonJs();
    }

    @And("User clicks delivery to button")
    public void userClicksDeliveryToButton() {
        homePage.clickDestinationCountryButton();
    }

    @And("User clicks select country dropdown")
    public void userClicksSelectCountryDropdown() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getDestinationCountryMenu());
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickDestinationCountryMenu();
    }

    @And("User choose country")
    public void userChooseCountry() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getUnitedKingdomRadioButton());
        homePage.clickUnitedKingdomRadioButton();
    }

    @When("User clicks Submit button")
    public void userClicksSubmitButton() {
        homePage.clickSubmitDestinationButton();
    }

    @Then("User checks destination country {string}")
    public void userChecksDestinationCountryCountry(final String country) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);

        try {
            homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        } catch (Exception e) {
            assertEquals(homePage.getDestinationCountryText(), country);
        }
    }

    @When("User clicks Sign-In button")
    public void userClicksSignInButton() {
        homePage.clickSignInButton();
    }

    @And("User input email {string}")
    public void userInputValidEmailValidEmail(final String email) {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.enterTextToEmailField(email);
    }


    @And("User clicks continue button")
    public void userClicksContinueButton() {
        signInPage.clickContinueButton();
    }

    @And("User input password {string}")
    public void userInputPassword(final String password) {
        signInPage.enterTextToPasswordField(password);
        try {
            signInPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        } catch (Exception ignored) {
        }
    }


    @Then("User checks alert about problem")
    public void userChecksAlertAboutProblem() {
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        signInPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, signInPage.alertMessage());
        assertTrue(signInPage.isAlertMessageDisplayed());
    }


    @And("User clicks change email button")
    public void userClicksChangeEmailButton() {
        signInPage.clickChangeEmailButton();
    }


    @And("User clicks Sign-In submit button")
    public void userClicksSignInSubmitButton() {
        signInPage.clickSignInSubmitButton();
    }


    @Then("User checks greeting {string} for logged in user")
    public void userChecksGreetingForLoggedInUser(final String greeting) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(homePage.getLoggedInUserGreetingsText(), greeting);
    }

    @And("User clicks SignOut button")
    public void userClicksSignOutButton() {
        homePage.hoverOverAccountsAndListsButton();
        homePage.clickSignOutButton();
    }

    @And("User checks login form header visibility")
    public void userChecksLoginFormHeaderVisibility() {
        signInPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, signInPage.emailField());
        assertTrue(signInPage.isSignInPageTitle());
    }

    @When("User clicks add to wishlist button")
    public void userClicksAddToWishlistButton() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.clickAddToListButton();
    }

    @Then("User checks message about successful  adding to list")
    public void userChecksMessageAboutSuccessfulAddingToList() {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getSuccessfulAddingToListMessage());
        assertTrue(productPage.isSuccessfulAddingToListMessageVisible());
    }

    @And("User clicks view list button")
    public void userClicksViewListButton() {
        productPage.clickViewListButton();
    }

    @And("User deletes added item from list")
    public void userDeletesAddedItemFromList() {
        wishlistPage = pageFactoryManager.getWishlistPage();
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.clickDeleteItemFromListButton();

    }

    @And("User checks that message about eliminating is visible")
    public void userChecksThatMessageAboutEliminatingIsVisible() {
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getItemDeletedMessage());
        assertTrue(wishlistPage.isItemDeletedMessageVisible());
    }

    @When("User clicks Create list button")
    public void userClicksCreateListButton() {
        homePage.hoverOverAccountsAndListsButton();
        homePage.clickCreateAListButton();
    }

    @Then("User checks shopping list title visibility")
    public void userChecksShoppingListTitleVisibility() {
        wishlistPage = pageFactoryManager.getWishlistPage();
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getNewListMenuHeader());
        assertTrue(wishlistPage.isNewListMenuHeaderVisible());
    }

    @And("User clicks create new list submit button")
    public void userClicksCreateNewListSubmitButton() {
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getNewListMenuHeader());
        wishlistPage.clickCreateNewListSubmitButton();
    }

    @And("User clicks menu more")
    public void userClicksMenuMore() {
        wishlistPage = pageFactoryManager.getWishlistPage();
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getMoreOptionsButton());
        wishlistPage.clickMoreOptionsButton();
    }

    @And("User clicks edit list button")
    public void userClicksEditListButton() {
        wishlistPage.clickEditListButton();
    }

    @And("User inputs new list name {string}")
    public void userInputsNewListName(final String newList) {
        wishlistPage.enterTextToListName(newList);
    }

    @And("User checks that list title  is {string}")
    public void userChecksThatListTitleIsNewListName(final String newList) {
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(wishlistPage.getShoppingListCreatedTitleText(), newList);
    }

    @And("User clicks delete button")
    public void userClicksDeleteButton() {
        wishlistPage.clickDeleteItemFromListButton();
    }

    @And("User clicks submit save button")
    public void userClicksSubmitSaveButton() {
        wishlistPage.clickSubmitSaveButton();
    }

    @And("User checks visibility of list name")
    public void userChecksVisibilityOfListName() {
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(wishlistPage.isShoppingListCreatedTitleVisible());
    }

    @When("User clicks shopping list menu")
    public void userClicksShoppingListMenu() {
        homePage.hoverOverAccountsAndListsButton();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCreateAShoppingListButton());
        homePage.clickCreateAShoppingListButton();
    }

    @And("User clicks idea list button")
    public void userClicksIdeaListButton() {
        wishlistPage = pageFactoryManager.getWishlistPage();
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.clickIdeaListButton();
    }

    @And("User clicks create idea list button")
    public void userClicksCreateIdeaListButton() {
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.clickCreateIdeaListButton();
    }

    @And("User inputs idea list name {string}")
    public void userInputsIdeaListName(final String ideaListName) {
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getIdeaListTitle());
        wishlistPage.enterTextToNewIdeaListField(ideaListName);
    }


    @And("User clicks idea submit button")
    public void userClicksIdeaSubmitButton() {
        wishlistPage.clickIdeaListSubmitButton();
    }

    @Then("User checks error message visibility")
    public void userChecksErrorMessageVisibility() {
        wishlistPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getIdeaListErrorMessage());
        assertTrue(wishlistPage.isIdeaListErrorMessageVisible());
    }

    @And("User click cart icon")
    public void userClickCartIcon() {
        homePage.pressEscape();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCartIcon());
        homePage.clickCartButton();
    }

    @And("User checks for message about deleted item")
    public void userChecksForMessageAboutDeletedItem() {
        shoppingCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(shoppingCartPage.isCartEmptyMessageVisible());

    }
}
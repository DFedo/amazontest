Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check site main functions presence
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    And User checks search field visibility
    And User checks cart menu visibility
    And User checks sign in menu visibility
    And User checks change language menu visibility
    And User checks deliver to button visibility
    When User clicks Store menu
    Then User checks that store menu is visible
    And User clicks close store menu button
    And User clicks cart button
    And User checks message that cart is empty
    Examples:
      | homePage                |
      | https://www.amazon.com/ |

  Scenario Outline: Check that search works correct
    Given User opens '<homePage>' page
    When User makes search by keyword '<keyword>'
    And User clicks search button
    Then User checks that needed keyword '<keyword>' is present on the page

    Examples:
      | homePage                | keyword         |
      | https://www.amazon.com/ | gaming headsets |

  Scenario Outline: Check that language and currency switches
    Given User opens '<homePage>' page
    And User clicks language button
    When User select language
    And User select currency
    And User clicks Save Changes Button
    Then User checks that Url contains 'de_DE' language
    And User makes search by keyword '<keyword>'
    And User clicks search button
    And User checks presence of 'EUR' currency

    Examples:
      | homePage                | keyword              |
      | https://www.amazon.com/ | CORSAIR VIRTUOSO RGB |


Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check site main functions presence
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    And User checks search field visibility
    And User checks cart menu visibility
    And User checks sign in menu visibility
    And User checks change language menu visibility
    And User checks deliver to button visibility
    When User clicks Store menu
    Then User checks that store menu is visible
    And User clicks close store menu button
    And User clicks cart button
    And User checks message that cart is empty
    Examples:
      | homePage                |
      | https://www.amazon.com/ |

  Scenario Outline: Check that search works correct
    Given User opens '<homePage>' page
    When User makes search by keyword '<keyword>'
    And User clicks search button
    Then User checks that needed keyword '<keyword>' is present on the page

    Examples:
      | homePage                | keyword         |
      | https://www.amazon.com/ | gaming headsets |

  Scenario Outline: Check that language and currency switches
    Given User opens '<homePage>' page
    And User clicks language button
    When User select language
    And User select currency
    And User clicks Save Changes Button
    Then User checks that Url contains 'de_DE' language
    And User makes search by keyword '<keyword>'
    And User clicks search button
    And User checks presence of 'EUR' currency

    Examples:
      | homePage                | keyword              |
      | https://www.amazon.com/ | CORSAIR VIRTUOSO RGB |

  Scenario Outline: Add to cart check
    Given User opens '<homePage>' page
    And User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on the first product
    When User clicks add to cart button
    And User click cart icon
    Then User checks  for message '<message 1>' about added to cart item
    And User delete added item from cart
    And User click cart icon
    And User checks for message about deleted item
    Examples:
      | homePage                | message 1          |  keyword          |
      | https://www.amazon.com/ | Subtotal (1 item): |  Razer BlackShark |


  Scenario Outline: Check delivery to
    Given User opens '<homePage>' page
    And User clicks delivery to button
    And User clicks select country dropdown
    And User choose country
    When User clicks Submit button
    Then User checks destination country '<country>'
    Examples:
      | homePage                | country        |
      | https://www.amazon.com/ | United Kingdom |

  Scenario Outline: Check alert while Login with invalid password
    Given User opens '<homePage>' page
    When User clicks Sign-In button
    And User input email '<valid email>'
    And User clicks continue button
    And User input password '<password>'
    And User clicks Sign-In submit button
    Then User checks alert about problem

    Examples:
      | homePage                | valid email          | password |
      | https://www.amazon.com/ | zim0000.fd@gmail.com | 123      |

  Scenario Outline: Login with valid credentials and logout check
    Given User opens '<homePage>' page
    When User clicks Sign-In button
    And User input email '<email>'
    And User clicks continue button
    And User input password '<password>'
    And User clicks Sign-In submit button
    Then User checks greeting '<greeting>' for logged in user
    And User clicks SignOut button
    And User checks login form header visibility
    Examples:
      | homePage                | email                | password   | greeting |
      | https://www.amazon.com/ | zim0000.fd@gmail.com | zim0000.fd | Hello, D |

  Scenario Outline: Check add to list
    Given User opens '<homePage>' page
    And User clicks Sign-In button
    And User input email '<email>'
    And User clicks continue button
    And User input password '<password>'
    And User clicks Sign-In submit button
    And User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on the first product
    When User clicks add to wishlist button
    Then User checks message about successful  adding to list
    And User clicks view list button
    And User deletes added item from list
    And User checks that message about eliminating is visible
    Examples:
      | homePage                | keyword          | email                | password   |
      | https://www.amazon.com/ | Razer BlackShark | zim0000.fd@gmail.com | zim0000.fd |

  Scenario Outline: New wishlist creation check
    Given User opens '<homePage>' page
    And User clicks Sign-In button
    And User input email '<email>'
    And User clicks continue button
    And User input password '<password>'
    And User clicks Sign-In submit button
    When User clicks Create list button
    Then User checks shopping list title visibility
    And User clicks create new list submit button
    And User checks visibility of list name
    Examples:
      | homePage                | email                | password   |
      | https://www.amazon.com/ | zim0000.fd@gmail.com | zim0000.fd |

  Scenario Outline: Check that new User can't Create idea list
    Given User opens '<homePage>' page
    And User clicks Sign-In button
    And User input email '<email>'
    And User clicks continue button
    And User input password '<password>'
    And User clicks Sign-In submit button
    When User clicks shopping list menu
    And User clicks idea list button
    And User clicks create idea list button
    And User inputs idea list name '<list name>'
    And User clicks idea submit button
    Then User checks error message visibility
    Examples:
      | homePage                | email                | password   | list name |
      | https://www.amazon.com/ | zim0000.fd@gmail.com | zim0000.fd | list1     |
